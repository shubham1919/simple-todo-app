import React, {Component} from 'react';
import { render } from 'react-dom';
import {Input,List,Skeleton,Avatar} from 'antd';





let input = '';
class TodoForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data : []
        }
    }

    addTodo=()=>{
        this.setState({
            data : this.state.data.concat([input]),
            input:''
        });
        
    }
    onInputChange=(e)=>{
        input = e.target.value;
    }
    remove=(i, e)=>{
        console.log(i);
        var array = [...this.state.data];
        array.splice(i,1);
        this.setState({
            data : array
        })
    }
    render(){
        const Todo = ()=>{
            let input;  
            return (
                <form onSubmit = {(e) =>{
                    e.preventDefault();
                    this.addTodo();}}>
                    <Input className="todoform-input-text" onChange={this.onInputChange} />
                </form>
            );
        };
        const Title = ({todoCount}) => {
            return (
              <div>
                 <div>
                    <h1>to-do ({todoCount})</h1>
                 </div>
              </div>
            );
          };

        const TodoList = ({todoData})=>{
            return (
                <List
                    size="small"
                    itemLayout="horizontal"
                    dataSource={todoData}
                    renderItem={(item, index) => (
                    <List.Item key={index}>
                                    <Skeleton avatar title={false} loading={item.loading} active>
              <List.Item.Meta
                title={<a href="#" key={index} onClick={this.remove.bind(this, index)}>{item}</a>}
              />
            </Skeleton>
                        </List.Item>)}
                />
            );
        };
        return(
            <div>
            <Title todoCount={this.state.data.length}></Title>
            <Todo></Todo>
            <TodoList todoData={this.state.data}></TodoList>
            </div>
        );
    }
}

export default TodoForm;